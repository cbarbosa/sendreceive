package send_receive_central;

public class Meio_send_ack extends Estado{
    public Meio_send_ack(Entidade _e){
        super(_e);
    }
    @Override
    public void transicao(Evento _ev){
        switch(_ev.code){
            case 6: // perde1
                // muda estado para IDLE
                ((Meio)ent).est =  ((Meio)ent)._idle;
                System.out.print("Perda1\n");
                break;
            case 4: // confirma
                // muda estado para IDLE
                ((Meio)ent).est =  ((Meio)ent)._idle;
                // gera evento CONFIRMA (entrega akc para SENDER)
                Evento e = new Evento(4,"confirma","");
                ((Meio)ent).s.colocaEvento(e);
            default: // evento inesperado               
                //((meio)ent).colocaEvento(_ev);
        }
    }
}
