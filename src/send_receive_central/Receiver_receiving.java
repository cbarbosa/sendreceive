package send_receive_central;

public class Receiver_receiving extends Estado{
     public Receiver_receiving (Entidade _e){
        super(_e);   
    }
    @Override
    public void transicao(Evento _ev){
        switch(_ev.code){
            case 10: //  recebe
                // entrega msg para o usuario
                System.out.print("Mensagem: "+_ev.msg+"\n");
                // muda estado para ACKNOWLEDGING
                ((Receiver)ent).est =  ((Receiver)ent)._acking;
                // gera evento RESPONDE para ACKN
                Evento e = new Evento(5,"responde"," ");
                 ((Receiver)ent).colocaEvento(e);
                break;
            default: // evento inesperado
                //((receiver)ent).colocaEvento(_ev);
        }
    }   
}
