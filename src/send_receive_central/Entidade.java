/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package send_receive_central;
import com.sun.jmx.remote.internal.ArrayQueue;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * @author Ciro Barbosa
 */
public class Entidade { 
   public ArrayQueue<Evento> buffer;
   public Estado est;
   public Entidade(){
       buffer = new ArrayQueue<Evento>(0xa);
   }
   public void transicao(Evento _e){
        if (est==null)
            ; //termina entidade
        else
            est.transicao(_e);
   }
   public Evento pegaEvento(){
       Evento e = null;
       if(buffer.size()>0){
           e = buffer.get(0);
           buffer.remove(0);
       }
       return e;
   }
   public void colocaEvento(Evento _e){
        buffer.add(_e);
   } 
   public void trabalha(){
        Evento e;
        while(true){            
            e = pegaEvento();
            while (e!=null){
                transicao(e);
                e=pegaEvento();                
            }
            try {
                Thread.sleep(70);
            } catch (InterruptedException ex) {
                Logger.getLogger(Entidade.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
