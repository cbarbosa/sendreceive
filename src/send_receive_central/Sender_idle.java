package send_receive_central;

public class Sender_idle extends Estado{
    public Sender_idle (Entidade _e){
        super(_e);   
    }
    @Override
    public void transicao(Evento _ev){
        switch(_ev.code){ 
            case 1: // msg
                // muda estado para SENDING
                ((Sender)ent).est=((Sender)ent)._sending;
                // gera evento ENVIA
                ((Sender)ent).colocaEvento(new Evento(2,"envia",((Sender)ent).msg));    
                break;                
            default: // evento inesperado
                //((Sender)ent).colocaEvento(_ev);
        }
    }
}
