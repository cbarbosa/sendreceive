/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package send_receive_central;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ciro Barbosa
 */
public class Timeout implements Runnable{
    Entidade ent;
    int tempo;
    boolean ativo;
    public Timeout(Entidade _ent, int _tempo){
        ent=_ent;
        tempo=_tempo;
    }
    public void paraTimer(){
        ativo=false;
    }
    @Override
    public void run(){
        ativo=true;
        try {
            Thread.sleep(tempo);
            if (ativo)
                ent.colocaEvento(new Evento(3,"timeout",""));
        } catch (InterruptedException ex) {
            Logger.getLogger(Timeout.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
