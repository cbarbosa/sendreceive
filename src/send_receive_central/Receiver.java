/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package send_receive_central;

/**
 *
 * @author Ciro Barbosa
 */
public class Receiver extends Entidade{
    public String msg;  
    public Meio m;
    public Estado _idle;
    public Estado _acking;
    public Estado _receiving;
    public Receiver(Entidade _m){
        m=(Meio)_m;
        _idle = new Receiver_idle(this);
        _acking = new Receiver_acking(this);
        _receiving = new Receiver_receiving(this);
        est = _idle;
    }
}
