package send_receive_central;

public class Meio_send_msg extends Estado{
    public Meio_send_msg(Entidade _e){
        super(_e);
    }
    @Override
    public void transicao(Evento _ev){
        switch(_ev.code){
            case 7: // perda2
                // muda estado para IDLE
                ((Meio)ent).est =  ((Meio)ent)._idle;
                System.out.print("Perda2\n");
                break;
            case 9: // entrega
                // muda estado para IDLE
                ((Meio)ent).est =  ((Meio)ent)._idle;
                // gera evento CONFIRMA (mensagem para RECEIVER
                Evento e = new Evento(9,"entrega",_ev.msg);
                ((Meio)ent).r.colocaEvento(e);
                break;
            default: // evento inesperado
                //((meio)ent).colocaEvento(_ev);
        }
    }
}
