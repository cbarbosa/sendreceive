package send_receive_central;

public class Receiver_acking extends Estado{
    public Receiver_acking (Entidade _e){
        super(_e);   
    }
    @Override
    public void transicao(Evento _ev){
        switch(_ev.code){
            case 5: // responde  
                // muda estado para IDLE
                ((Receiver)ent).est =  ((Receiver)ent)._idle;
                // gera evento RESPONDE para o MEIO
                Evento e = new Evento(5,"responde","");
                ((Receiver)ent).m.colocaEvento(e);
                break;
            default: // evento inesperado
                //((receiver)ent).colocaEvento(_ev);
        }
    }   
}
