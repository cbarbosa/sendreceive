/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package send_receive_central;

/**
 *
 * @author Ciro Barbosa
 */
public class Meio extends Entidade{
 public String msg;  
    public Sender s;
    public Receiver r;
    public Estado _send_ack;
    public Estado _idle;
    public Estado _send_msg;
    public void setSender(Entidade _e){
        s=(Sender)_e;
    }
    public void setReceiver(Entidade _e){
        r=(Receiver)_e;
    }
    public Meio(){
        _idle = new Meio_idle(this);
        _send_ack = new Meio_send_ack(this);
        _send_msg = new Meio_send_msg(this);
        est = _idle;
    }
}
