/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package send_receive_central;
/**
 * @author Ciro Barbosa
 */
public class Sender extends Entidade{
    public String msg;  
    public Meio m;
    public Estado _idle;
    public Estado _sending;
    public Estado _waiting;
    Thread thread1;
    Timeout t1;
    public Sender(Entidade _m){
        // inicializa objeto timeout
        t1 = new Timeout(this, 10000);
        m=(Meio)_m;
        _idle = new Sender_idle(this);
        _sending = new Sender_sending(this);
        _waiting = new Sender_waiting(this);
        est = _waiting;
    }
  
}
