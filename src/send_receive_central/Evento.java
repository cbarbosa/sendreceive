package send_receive_central;
/*
 * @author Ciro Barbosa
 */
public class Evento {
    public int code;
    public String nome;
    public String msg;
    public Evento(int _c, String _n, String _m){
        code=_c;
        nome=_n;
        msg=_m;
    }
}
