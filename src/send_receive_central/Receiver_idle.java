package send_receive_central;

public class Receiver_idle extends Estado{
    public Receiver_idle (Entidade _e){
        super(_e);   
    }
    @Override
    public void transicao(Evento _ev){
        switch(_ev.code){
            case 9: // entrega
                // guarda msg
                ((Receiver)ent).msg = _ev.msg;
                // muda estado para RECEIVING
                ((Receiver)ent).est =  ((Receiver)ent)._receiving;
                // gera evento RECEBE
                Evento e = new Evento(10,"recebe",((Receiver)ent).msg);
                ((Receiver)ent).colocaEvento(e);
                break;
            default: // evento inesperado
                //((receiver)ent).colocaEvento(_ev);
        }
    }
}
