package send_receive_central;

public class Meio_idle extends Estado{
    public Meio_idle (Entidade _e){
        super(_e);
    }
    @Override
    public void transicao(Evento _ev){
        switch(_ev.code){
            case 2: // envia
                // guarda msg
                ((Meio)ent).msg = _ev.msg;
                // muda estado para SEND_MSG
                ((Meio)ent).est =  ((Meio)ent)._send_msg;
                // gera evento (perda2 ou entrega)
                    // decide se perde ou se entrega
                    if ((1 + (int)(Math.random() * 100)) > 10){
                        // gera ENTREGA 
                        Evento e = new Evento(9,"entrega",((Meio)ent).msg);
                        ((Meio)ent).colocaEvento(e);
                    }
                    else{
                        // gera PERDA2
                        Evento e = new Evento(7,"perda2",((Meio)ent).msg);
                        ((Meio)ent).colocaEvento(e);
                    }
                break;
            case 5: // reponde
                // muda estado para SEND_ACK
                ((Meio)ent).est =  ((Meio)ent)._send_ack;
                // gera PERDA1 ou CONFIRMA
                // decide se perde ou se confirma
                if ((1 + (int)(Math.random() * 100)) > 10){
                    // confirma 
                    Evento e = new Evento(4,"confirma","");
                    ((Meio)ent).colocaEvento(e);
                }
                else{
                    // perda1
                    Evento e = new Evento(6,"perda1","");
                    ((Meio)ent).colocaEvento(e);
                }
                break;
            default:// evento inesperado
                //((meio)ent).colocaEvento(_ev);
        }
    }
}
