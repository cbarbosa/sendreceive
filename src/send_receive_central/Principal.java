/*
 *  Implementacao do Protocolo Send/Receive
 *  Exemplo de uso do framework para implementação de protocolos
 *  Universidade Federal de Juiz de Fora
 *  Departamento de Ciência da Computação
 */
package send_receive_central;
/**
 * @author Ciro Barbosa
 *  * 1: msg
 *    2: envia
 *    3: timeout
 *    4: confirma
 *    5: responde
 *    6: perde1
 *    7: perde2
 *    9: entrega
 *   10: recebe
 */
public class Principal {
    public static final int msg = 1;
    public static final int envia = 2;
    public static final int timeout = 3;
    public static final int confirma = 4;
    public static final int responde = 5;
    public static final int perde1 = 6;
    public static final int perde2 = 7;
    public static final int entrega = 8;
    public static final int recebe = 9;
    Meio m;
    public static void main(String args[]) {
        Principal p = new Principal();
        p.inicia();
        p.envia();
    }
    public void envia(){
            // 'Confirma' faz SENDER pedir mensagem
            Evento e = new Evento(confirma,"confirma","");
            m.s.colocaEvento(e);   
    }
    public void inicia(){
        // Instancia entidades do protocolo
        m = new Meio();
        m.setSender(new Sender(m));
        m.setReceiver(new Receiver(m));
        // Dispara thread
        new Thread (new SThread(m.s)).start();
        new Thread (new SThread(m.r)).start();
        new Thread (new SThread(m)).start();
    } 
}
