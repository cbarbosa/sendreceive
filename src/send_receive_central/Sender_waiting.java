package send_receive_central;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Sender_waiting extends Estado{
       public Sender_waiting(Entidade _e){
        super(_e);
    }
    @Override
    public void transicao(Evento _ev){
        switch(_ev.code){
            case 3: // timeout
                // Informa Timeout
                System.out.println("Timeout");
                // muda estado para SENDING
                ((Sender)ent).est = ((Sender)ent)._sending;
                // gera evento ENVIA
                ((Sender)ent).colocaEvento(new Evento(2,"envia",((Sender)ent).msg)); 
                break;
            case 4: // confirma
                 // para timer
                 ((Sender)ent).t1.paraTimer();                    
                 // muda estado para IDLE
                 ((Sender)ent).est = ((Sender)ent)._idle;
                 // le dado do usuário 
                 ((Sender)ent).msg = le();
                 // gera evento MSG
                 ((Sender)ent).colocaEvento(new Evento(1,"msg",""));
                break;   
            default:
                //((sender)ent).colocaEvento(_ev);
        }
    }
        public String le(){
        String aux;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); 
        System.out.print("Informe a menssagem: ");
        try {
            aux = in.readLine();
            return aux;
        } catch (IOException ex) {
            Logger.getLogger(Sender_idle.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
